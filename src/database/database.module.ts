import { Module } from '@nestjs/common';
import { RedisModule } from 'nestjs-redis';

@Module({
  imports: [
    RedisModule.register({
      name: 'test',
      url: 'redis://localhost:6379'
    })
  ],
})
export class DatabaseModule {}
