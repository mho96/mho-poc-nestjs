import { IsString, IsInt, MinLength } from 'class-validator';

export class CreateContentDto {
    @IsString()
    @MinLength(10)
    public title: string;

    @IsString()
    public body: string;
}
