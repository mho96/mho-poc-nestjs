import { Controller, Get, Param, NotFoundException, Post, Body, InternalServerErrorException, UsePipes, ValidationPipe, Put, Patch, Delete, Request, UseGuards } from '@nestjs/common';
import { ContentService } from './content.service';
import { Content } from './interface/content.interface';
import { CreateContentDto } from './dto/content';
import * as uniqid from 'uniqid';
import { RolesGuard } from 'src/guards/roles.guard';
import { Roles } from 'src/decorators/roles.decorator';

@Controller('content')
export class ContentController {
    constructor(readonly contentService: ContentService) {}

    @Get('/:id')
    async getOne(@Param('id') id: string): Promise<Content> {
        const content = await this.contentService.getOne(id);
        if (content === null) {
            throw new NotFoundException();
        }

        return JSON.parse(content);
    }


    @Get('')
    @Roles('admin', 'toto')
    @UseGuards(RolesGuard)
    async getMany(@Request() req: Request): Promise<Content[]> {
        console.log(req)
        const contents = await this.contentService.getMany();
        if (contents === null) {
            throw new NotFoundException();
        }

        return Object.keys(contents).map(val => {
            return {
                id: val,
                ...JSON.parse(contents[val])
            }
        });
    }

    @Post('')
    @UsePipes(new ValidationPipe({
        whitelist: true,
        transform: true
    }))
    async create(@Body() payload: CreateContentDto): Promise<Content> {
        try {
            const id: string = uniqid();
            const fields = {...payload, created: Math.floor(Date.now()/1000)};
            const result = await this.contentService.create(id, fields);
            if (result) {
                throw new InternalServerErrorException('redis didnt create the content');
            }
            return this.getOne(id);
        } catch (err) {
            throw new InternalServerErrorException(err.message);
        }
    }

    @Put('/:id')
    @UsePipes(new ValidationPipe({
        whitelist: true,
        transform: true
    }))
    async replace(
        @Param('id') id: string,
        @Body() payload: CreateContentDto
    ): Promise<Content> {
        const fields = {
            ...payload,
            created: Math.floor(Date.now()/1000)
        };
        const content = await this.contentService.create(id, fields);
        if (content === null) {
            throw new NotFoundException();
        }

        return this.getOne(id);
    }

    @Patch('/:id')
    @UsePipes(new ValidationPipe({
        skipMissingProperties: true,
        whitelist: true,
        transform: true
    }))
    async update(
        @Param('id') id: string,
        @Body() payload: CreateContentDto
    ): Promise<Content> {
        const existing = await this.getOne(id);
        const fields = { 
            ...existing,
            ...payload,
            created: Math.floor(Date.now()/1000)
        };
        await this.contentService.create(id, fields);
        return fields;
    }

    @Delete('/:id')
    async remove(@Param('id') id: string): Promise<object> {
        const result = await this.contentService.delete(id);
        const message = result 
        ? `content ${id} has been deleted`
        : `content ${id} has not been deleted`
        return { message };
    }

}
