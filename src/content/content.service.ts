import { Injectable } from '@nestjs/common';
import { Content } from './interface/content.interface';
import { RedisService } from 'nestjs-redis';

@Injectable()
export class ContentService {
    private contentSet: string;
    private client: any;
    constructor(readonly databaseService: RedisService) {
        this.contentSet = 'contents';
        this.client = this.databaseService.getClient('test');
    }

    getOne(id: string): Promise<string> | null {
        return this.client.hget(this.contentSet, id);
    }

    getMany(): Promise<string> | null {
        return this.client.hgetall(this.contentSet);
    }

    create(id: string, content: Content): Promise<number> | null {
        return this.client.hset(
            this.contentSet, id, JSON.stringify(content)
        );
    }

    delete(id: string): Promise<number> {
        return this.client.hdel(this.contentSet, id);
    }
}
