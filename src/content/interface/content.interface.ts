export interface Content {
    title: string,
    body: string,
    created: number,
}
