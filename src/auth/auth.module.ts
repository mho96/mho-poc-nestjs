import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConst } from './constants';
import { JwtStrategy } from './jwt.strategy';

@Module({
    imports: [
        PassportModule,
        JwtModule.register({
            secret: jwtConst.secret,
            verifyOptions: {
                algorithms: ['HS256']
            }
        })
    ],
    providers: [JwtStrategy]
})
export class AuthModule {}
