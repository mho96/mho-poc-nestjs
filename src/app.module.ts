import { Module } from '@nestjs/common';
import { ContentModule } from './content/content.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [ContentModule, AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
